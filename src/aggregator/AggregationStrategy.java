package aggregator;

import java.util.List;

public interface AggregationStrategy {

	public List<SearchResult> execute(String phrase, List<ISearchProvider> providers) throws Exception;
	
}
