package aggregator;

import java.util.ArrayList;
import java.util.List;

public class AppTry {
	public static void main(String[] args) throws Exception {
		
		List<ISearchProvider> providers = new ArrayList<ISearchProvider>();
		providers.add(new BingSearchprovider());
		MultiSearcher ms = new MultiSearcher(providers);
		
		List <SearchResult> result = ms.performSearch("github", new UniqueAggregationStrategy());
		
		for (SearchResult res : result) {
			System.out.println(res.Description);			
		}
	}
}
