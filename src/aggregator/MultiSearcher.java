package aggregator;

import java.util.List;

class MultiSearcher { // МАШИНА
	List<ISearchProvider> providers;

	public MultiSearcher(List<ISearchProvider> providers) {
        this.providers = providers;
    }

	public List<SearchResult> performSearch(String phrase, AggregationStrategy strategy) throws Exception {
        return strategy.execute(phrase, providers);
    }
}
