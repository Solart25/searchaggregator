package aggregator;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Base64;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

public class BingSearchprovider implements ISearchProvider {

	private static String accountId;
	private static String searchPhrase;
	private static int size;
	String searchResult;	

	public List<SearchResult> search(String phrase) throws Exception {
				
		searchPhrase = phrase;
		size = 10;
		accountId = Config.getBingID();
		// Paging requires the use of $top and $size query params
		final String bingUrlPattern = "https://api.datamarket.azure.com/Bing/Search/Web?$top=" + size + "&$skip=0&$format=JSON&Query=%%27%s%%27";
		final String query = URLEncoder.encode(searchPhrase, Charset.defaultCharset().name());
		final String bingUrl = String.format(bingUrlPattern, query);
		final String accountKeyEnc = Base64.getEncoder().encodeToString((accountId + ":" + accountId).getBytes());
		final URL url = new URL(bingUrl);
		final URLConnection connection = url.openConnection();
		connection.setRequestProperty("Authorization", "Basic " + accountKeyEnc);
		
		BingResult results = null;
		try (final BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {

			Gson gson = new Gson();
			results = gson.fromJson(in, BingResult.class);			
			in.close();

		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<SearchResult>();
		}
		return results.d.results;
	}
		
	public class BingResult {
		D d;
	}

	class D {
		List<SearchResult> results;
	}
}


