package aggregator;

import java.util.List;

public interface ISearchProvider {

	public List<SearchResult> search(String phrase) throws Exception;

}
