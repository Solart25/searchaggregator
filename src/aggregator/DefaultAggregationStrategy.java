package aggregator;

import java.util.ArrayList;
import java.util.List;

public class DefaultAggregationStrategy implements AggregationStrategy {

	@Override
	public List<SearchResult> execute(String phrase, List<ISearchProvider> providers) throws Exception {
		
		List <SearchResult> searchResults = new ArrayList<SearchResult>();
		
		for(ISearchProvider provider : providers) {
			searchResults.addAll(provider.search(phrase));
		} 
				
		return searchResults;
	}

}
